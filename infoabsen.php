<?php include 'header.php';?>
<!-- Absen Keluar -->
<!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand">Daftar Absensi</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
        </div>
      </nav>
<!-- End Navbar -->
<div class="panel-header panel-header-sm">
      </div>
      <div class="content">
        <div class="row justify-content-center">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header text-center">
                <h5 class="title">Daftar Absensi</h5>
              </div>
              <div class="card-body">
              	<form action="" method="post">
	                <div class="row">
	                	<div class="col-md-7">
	                		<div class="form-group">
	                			<?php
	                			 if (isset($_POST['tanggal'])){
	                			 	echo "<input type=\"date\" class=\"form-control\" name=\"tanggal\" value=\"".$_POST['tanggal']."\" required>";
	                			 }else{
	                			 	echo "<input type=\"date\" class=\"form-control\" name=\"tanggal\" required>";
	                			 }
	                			?>
	                		</div>
	                	</div>
	                	<div class="col-md-3">
	                		<div class="form-group">
	                			<input type="submit" class="form-control btn-primary " name="submit" value="Filter">
	                		</div>
	                	</div>
	                </div>
                </form>
                <div class="row">
                	<div class="col-md-12">
                		<div class="table-responsive">
                  <table class="table">
                    <thead class=" text-info">
                      <th >
                        No
                      </th>
                      <th>
                        Nama
                      </th>
                      <th>
                        Masuk
                      </th>
                      <th>
                        Pulang
                      </th>
                    </thead>
                    <tbody>
                    	<?php
                    	$date = "";
                    	if (isset($_POST['tanggal'])) {
                    		$date = date("Y-m-d",strtotime($_POST['tanggal']));
                    	}
                    	$no = 1;
                    	$data = listAbsensi($date);
                    	// echo json_encode($data);
                    	if (!empty($data)) {
                    		for ($i = 0; $i < count($data); $i++) {
	                    		echo '<tr>';
	                    	    echo '<td>'.$no++.'</td>';
	                    	    echo '<td>'.$data[$i]['nama'].'</td>';
	                    	    if (empty($data[$i]['absen_masuk'])) {
	                    	    	echo '<td> - </td>';
	                    	    }else{
	                    	    	echo '<td>'.date("d/m/Y",strtotime($data[$i]['absen_masuk'])).'</td>';
	                    	    }
	                    	    if (empty($data[$i]['absen_pulang'])) {
	                    	    	echo '<td> - </td>';
	                    	    }else{
	                    	    	echo '<td>'.date("d/m/Y",strtotime($data[$i]['absen_pulang'])).'</td>';
	                    	    }
	                    	    echo '</tr>';
                    		}
                    	}
                    	?>
                    </tbody>
                  </table>
                </div>
                	</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php include 'footer.php';?>