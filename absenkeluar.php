<?php include 'header.php';?>
<!-- Absen Keluar -->
<!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand">Absensi Pulang</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
        </div>
      </nav>
<!-- End Navbar -->
<div class="panel-header panel-header-sm">
      </div>
      <div class="content">
        <div class="row justify-content-center">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header text-center">
                <h5 class="title">Absensi Pulang</h5>
              </div>
              <div class="card-body">
                <form action="" method="post" name="form">
                  <div class="row justify-content-center">
                  	<div class="col-md-10">
	                  	<div class="form-group">
	                  		<input type="number" name="nim" class="form-control" placeholder="NIM" required/>
	                  	</div>
                  	</div>
                  </div>
                  <div class="row justify-content-center">
                  	<div class="col-md-10">
                  	<div class="form-group">
                  			<input type="submit" class="form-control btn btn-primary" name="submit" value="Absen">
	                  	</div>
                  	</div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <?php
      	if (isset($_POST['submit'])) {
      		$nim = $_POST['nim'];
      		$result = absenKeluar($nim);
      		if (!empty($result)) {
      			echo "<script type='text/javascript'>alert('".$result['message']."');</script>";
      		}else{
      			echo "<script type='text/javascript'>alert('gagal menjalankan fungsi');</script>";
      		}
      	}
      ?>
<?php include 'footer.php';?>