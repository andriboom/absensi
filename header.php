<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Absensi
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/now-ui-dashboard.css?v=1.3.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="assets/demo/demo.css" rel="stylesheet" />
</head>
<body class="">
  <?php
include_once('process/database_function.php');
$parts = explode('/', $_SERVER['REQUEST_URI']);
$position = '';
if(!empty($parts[2])){
  if ($parts[2] == 'index.php') {
    $position = 'index'; 
  }elseif($parts[2] == 'absenmasuk.php'){
    $position = 'absenmasuk';
  }else if ($parts[2] == 'absenkeluar.php') {
    $position = 'absenkeluar';
  }elseif ($parts[2] == 'infoabsen.php') {
    $position = 'infoabsen';
  }
}
?>
	<div class="wrapper ">
    <div class="sidebar" data-color="blue">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
      	<div class="simple-text logo-normal"> Absensi </div>
        
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          <?php
          	if($position == 'index'){
          		echo "<li  class=\"active \">";
          	}else{
          		echo "<li>";
          	}
          ?>
            <a href="./index.php">
              <i class="now-ui-icons design_app"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <?php
          	if($position == 'absenmasuk'){
          		echo "<li  class=\"active \">";
          	}else{
          		echo "<li>";
          	}
          ?>
            <a href="./absenmasuk.php">
              <i class="now-ui-icons business_badge"></i>
              <p>Absen Masuk</p>
            </a>
          </li>
          <?php
          	if($position == 'absenkeluar'){
          		echo "<li  class=\"active \">";
          	}else{
          		echo "<li>";
          	}
          ?>
            <a href="./absenkeluar.php">
              <i class="now-ui-icons business_badge"></i>
              <p>Absen Keluar</p>
            </a>
          </li>
          <?php
          	if($position == 'infoabsen'){
          		echo "<li  class=\"active \">";
          	}else{
          		echo "<li>";
          	}
          ?>
            <a href="./infoabsen.php">
              <i class="now-ui-icons design_bullet-list-67"></i>
              <p>Info Absensi Hari Ini</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel" id="main-panel">
      
	
