<?php
	date_default_timezone_set('Asia/Jakarta');
	//mysqli connection start
	$databaseHost = 'localhost';
	$databaseName = 'absensi';
	$databaseUsername = 'root';
	$databasePassword = '';

	$mysqli = mysqli_connect($databaseHost, $databaseUsername, $databasePassword, $databaseName); 
	//mysqli connection end
	
	function absenMasuk($nim)
	{		
		$checkNim = mysqli_fetch_row(queryDB("select id_mahasiswa from mahasiswa where nim=".$nim));
		if (!empty($checkNim)) {
			$checkAbsen = mysqli_fetch_row(queryDB("
				select * 
				from absensi 
				where 
				id_mahasiswa=".$checkNim[0]." 
				AND DATE(absen_masuk)='".date("Y-m-d")."'"));
			if (empty($checkAbsen)) {
				$insert = queryDB("INSERT INTO absensi(id_mahasiswa,absen_masuk) VALUES('".$checkNim[0]."','".date("Y-m-d H:i:s")."')");
				if ($insert) {
					$output['status'] = true;
					$output['message'] = "Berhasil absen masuk";
				}else{
					$output['status'] = false;
					$output['message'] = "Terjadi kesalahan menyimpan data";
				}
			}else{
				$output['status'] = false;
				$output['message'] = "Mahasiswa telah absen sebelumnya";
			}
		}else{
			$output['status'] = false;
			$output['message'] = "Mahasiswa tidak ditemukan";
		}
		return $output;
	}

	function absenKeluar($nim)
	{
		$checkNim = mysqli_fetch_row(queryDB("select id_mahasiswa from mahasiswa where nim=".$nim));
		if (!empty($checkNim)) {
			$checkAbsen = mysqli_fetch_row(queryDB("
				select id_absensi,absen_pulang
				from absensi 
				where 
				id_mahasiswa=".$checkNim[0]." 
				AND DATE(absen_masuk)='".date("Y-m-d")."'"));
			if (!empty($checkAbsen)) {
				if (empty($checkAbsen[1])) {
					$update = queryDB("UPDATE absensi SET absen_pulang='".date("Y-m-d H:i:s")."' WHERE id_absensi=".$checkAbsen[0]);
					if ($update) {
						$output['status'] = true;
						$output['message'] = "Berhasil absen pulang";
					}else{
						$output['status'] = false;
						$output['message'] = "Terjadi kesalahan menyimpan data";
					}
				}else{
					$output['status'] = false;
					$output['message'] = "Mahasiswa telah mengisi absen pulang";
				}
			}else{
				$output['status'] = false;
				$output['message'] = "Mahasiswa belum mengisi absensi saat masuk";
			}
		}else{
			$output['status'] = false;
			$output['message'] = "Mahasiswa tidak ditemukan";
		}	
		return $output;
	}

	function listAbsensi($date='')
	{
		global $mysqli;
		if (!empty($date)) {
			$query = "select * from absensi 
			JOIN mahasiswa ON mahasiswa.id_mahasiswa=absensi.id_mahasiswa 
			where DATE(absen_masuk)='".$date."'";
			$result = $mysqli->query($query);
		}else{
			$query = "select * from absensi 
			JOIN mahasiswa ON mahasiswa.id_mahasiswa=absensi.id_mahasiswa 
			where DATE(absen_masuk)='".date("Y-m-d")."'";
			$result = $mysqli->query($query);
		}
		if ($result->num_rows>0) {
				while($row = $result->fetch_assoc()) {
					$data[] = $row; 
				}
				return $data;
			}else{
				return [];
			}
		 // 
		// return $data;
	}

	function queryDB($query)
	{
		global $mysqli;
		$result = mysqli_query($mysqli,$query);
		// mysqli_close($mysqli);
		return $result;
	}

?>